import logo from './logo.svg';
import './App.css';

function App() {

  const dataFile = "testData_1.json"

  const handleButtonClick = () => {
    console.log("You take over from here.")
  }

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          React Coding Evaluation
        </p>
        <label
          style={{
            color: "#4737F6",
            border: "1px solid #4737F6",
            borderRadius: "8px",
            padding: "5px 10px",
            outline: "none",
            marginRight: "10px",
            marginLeft: "10px",
            cursor: "pointer",
            fontSize: "12px",
            float: "right"
          }}>
                 <span>
                    <div onClick={() => {
                      handleButtonClick(true)
                    }}>Adjust WIP
                    </div>
                </span>
        </label>
      </header>
    </div>
  );
}

export default App;
